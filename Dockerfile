FROM node:8.8.1-alpine as builder
MAINTAINER Ozzy Ndiaye <snekshaark@gmail.com>
ARG APP_VERSION
LABEL version=${VERSION}

RUN mkdir -p /app
WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install --no-progress --ignore-scripts --emoji # (:

COPY . .

FROM node:8.8.1-alpine
ENV NODE_ENV "production"
ENV APP_PORT 5000
EXPOSE 5000

COPY --from=builder /app .
CMD [ "node", "." ]

