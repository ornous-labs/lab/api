module.exports = () => [
  {
    "id": 1,
    "name": "Bass Practice",
    "duration": "90 minutes",
    "frequency": "DAILY",
    "props": [
      { id: 1, name: "Bass Guitar", qty: 1 },
      { id: 2, name: "Bass Amp", qty: 1 },
      { id: 3, name: "1/4\" Lead", qty: 1 },
      { id: 4, name: "Picks", qty: 1 },
      { id: 5, name: "Headphones", qty: 1 },
      { id: 6, name: "Bass Partitions", qty: 1 },
    ],
    "schedule": [
      { id: 1, name: "Mise en Place", duration: "5 minutes" },
      { id: 2, name: "Warm Up", duration: "5 minutes" },
      { id: 3, name: "Scales", duration: "10 minutes" },
      { id: 4, name: "Tune Practice", duration: "15 minutes" },
      { id: 5, name: "Return to Neutral", duration: "5 minutes" }
    ]
  },
]
