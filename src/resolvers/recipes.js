module.exports = () => [
  {
    id: 1,
    name: "Lemon Drizzle Cake",
    ingredients: [
      { name: "Eggs", quantity: 4 }
    ],
    instructions: [
      "Make it good"
    ],
    yield: 1,
    prepTime: 1,
    cookTime: 1,
    suitableForDiets: [],
    tools: []
  }
]
