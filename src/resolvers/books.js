module.exports = () => [
  {
    id: 1,
    title: "Factfulness",
    subtitle: "Ten Reasons We're wrong about the World - and Why Things Are Better Than You Think",
    authors: [
      { id: 1, name: "Hans Rosling" }
    ],
    identifiers: [
    ],
    publishYear: 2018,
  }
]
