const routines = require('./routines')
const recipes = require('./recipes')
const books = require('./books')

module.exports = {
  Query: {
    books,
    recipes,
    routines
  }
}
