const compression = require('compression')
const express = require('express')
const morgan = require('morgan')
const { ApolloServer } = require('apollo-server-express')

const schema = require('./schema')
const resolvers = require('./resolvers')
const server = new ApolloServer({ typeDefs: schema, resolvers })

const { logger, standardQueryLogger } = require('./utils/logger')

const { APP_PORT = 5000, APP_HOST = '0.0.0.0' } = process.env

const app = express()

app.set('host', APP_HOST)
app.set('port', APP_PORT)

app.use(compression())
app.use(standardQueryLogger())
server.applyMiddleware({ app })

app.get('/healthz', (req, res) => {
  res.writeHead(200)
  return res.end('ok')
})

app.on('listening', () => {
  const host = app.get('host')
  const port = app.get('port')

  logger.info(`Express server listening on ${host}:${port}`)
  logger.info(`GraphQL playground started at ${host}:${port}/graphql`)
})

app.on('close', () => logger.info('Server was closed'))

app.start = () => app.listen({
  port: app.get('port'),
  host: app.get('host')
}, () => app.emit('listening'))

module.exports = app
