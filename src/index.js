const http = require('http')
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const { logger } = require('./utils/logger')
const app = require('./app')
const server = http.createServer(app)

server.listen(5000, "0.0.0.0", () => {
  logger.info("Server Started")
})

process.on('uncaughtException', (err) => {
  process.removeListener('uncaughtException', arguments.callee)

  console.log({ err })
  logger.error({ err }, 'Uncaught Exception')
  process.exit(1)
})

process.on('unhandledRejection', (reason, promise) => {
  process.removeListener('unhandledRejection', arguments.callee)

  logger.error({ reason, promise }, 'Unhandled Rejection')
  process.exit(1)
});

process.on('SIGTERM', () => {
  logger.info('Attempting graceful shutdown')
  server.close((err) => {
    if (err) {
      logger.error({ err }, 'Unable to gracefully shutdown (express)')
      process.exit(1)
    }
  })

  logger.info("Shutdown Complete")
  process.exit()
})
