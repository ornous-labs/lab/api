const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type Query {
    routines: [Routine!]
    recipes: [Recipe!]
    books: [Book!]
  }

  type Routine {
    id: ID!
    name: String!
    duration: String!
    frequency: String!
    props: [Prop!]
    schedule: [Activity!]!
  }

  type Activity {
    id: ID!
    name: String!
    duration: String!
    # props?
  }

  type Recipe {
    id: ID!
    name: String!
    ingredients: [Ingredient!]
    instructions: [Instruction!]
    yield: Int!
    prepTime: Duration!
    cookTime: Duration!
    suitableForDiets: [Diet!]
    tools: [Prop!]!
  }

  scalar Duration
  scalar Diet
  scalar Instruction
  scalar Quantity
  scalar Url

  type Author {
    id: ID!
    name: String!
  }

  type Link {
    title: String!
    url: Url!
  }

  type Ingredient {
    name: String!
    quantity: Quantity
  }

  type Prop {
    name: String!
    quantity: Int!
  }

  type Book {
    id: ID!
    title: String!
    subtitle:String!
    authors: [Author!]
    cover: String
    thumbnailUrl: String
    identifiers: [String!]
    excerpts: [String!]
    links: [Link!]
    numberOfPages: Int
    publishYear: Int!
  }
`

module.exports = typeDefs
