const winston = require('winston')
const morgan = require('morgan')
const appRoot = require('app-root-path')

// Move to config
const options = {
  console: {
    level: 'info',
    handleExceptions: true,
    json: true,
    format: winston.format.combine(
      winston.format.colorize({ all: true }),
      winston.format.simple()
    )
  }
}

const logger = winston.createLogger({
  transports: [],
  exitOnError: false
})

if (process.env.NODE_ENV !== 'production') {
  options.console.json = false
  options.console.level = "debug"
}

logger.add(new winston.transports.Console(options.console))

const standardQueryLogger = () =>
  morgan('combined', { stream: { write: message => logger.info(message) } })

module.exports = {
  logger,
  standardQueryLogger,
}
